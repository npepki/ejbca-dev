/*************************************************************************
 *                                                                       *
 *  EJBCA Community: The OpenSource Certificate Authority                *
 *                                                                       *
 *  This software is free software; you can redistribute it and/or       *
 *  modify it under the terms of the GNU Lesser General Public           *
 *  License as published by the Free Software Foundation; either         *
 *  version 2.1 of the License, or any later version.                    *
 *                                                                       *
 *  See terms of license at gnu.org.                                     *
 *                                                                       *
 *************************************************************************/

package org.ejbca.ui.cli.ca;

import java.text.SimpleDateFormat;

import java.util.Date;

import org.apache.log4j.Logger;
import org.cesecore.certificates.ca.CADoesntExistsException;
import org.cesecore.certificates.ca.CAInfo;
import org.cesecore.certificates.certificate.CertificateConstants;
import org.cesecore.util.CryptoProviderTools;
import org.cesecore.util.EjbRemoteHelper;
import org.ejbca.ui.cli.infrastructure.command.CommandResult;
import org.ejbca.ui.cli.infrastructure.parameter.Parameter;
import org.ejbca.ui.cli.infrastructure.parameter.ParameterContainer;
import org.ejbca.ui.cli.infrastructure.parameter.enums.MandatoryMode;
import org.ejbca.ui.cli.infrastructure.parameter.enums.ParameterMode;
import org.ejbca.ui.cli.infrastructure.parameter.enums.StandaloneMode;

import org.cesecore.certificates.ca.*;
import org.ejbca.core.ejb.crl.*;
import org.cesecore.certificates.crl.*;

/**
 * Issues a new CRL from the CA.
 *
 * @version $Id: CaCreateCrlCommand.java 32405 2019-05-23 14:01:14Z andrey_s_helmes $
 */
public class CaCreateCrlCommand extends BaseCaAdminCommand {

    private static final Logger log = Logger.getLogger(CaCreateCrlCommand.class);

    private static final String CA_NAME_KEY = "--caname";
    private static final String DELTA_KEY = "-delta";
    private static final String UPDATE_DATE = "--updateDate";

    {
        registerParameter(new Parameter(CA_NAME_KEY, "CA Name", MandatoryMode.OPTIONAL, StandaloneMode.ALLOW, ParameterMode.ARGUMENT,
                "If no caname is given, CRLs will be created for all the CAs where it is neccessary."));
        registerParameter(new Parameter(DELTA_KEY, "", MandatoryMode.OPTIONAL, StandaloneMode.FORBID, ParameterMode.FLAG,
                "Set if a Delta CRL is desired"));
        registerParameter(new Parameter(UPDATE_DATE, "", MandatoryMode.OPTIONAL, StandaloneMode.FORBID, ParameterMode.ARGUMENT,
                "Set a custom update date (start date) for issuing CRLs for the future."));
    }

    @Override
    public String getMainCommand() {
        return "createcrl";
    }

    Date udate = null;

    @Override
    public CommandResult execute(ParameterContainer parameters) {
        String caName = parameters.get(CA_NAME_KEY);
        boolean deltaCrl = parameters.get(DELTA_KEY) != null;

        final String updateDate = parameters.get(UPDATE_DATE);
        if ( updateDate != null ) {
        	SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        	try {
        		udate = format.parse( updateDate );
        	} catch ( Exception e ) {
        		throw new RuntimeException(e.getMessage());
        	}
        }
 
        if (caName == null) {
            createCRL(null, deltaCrl);
        } else {
            CryptoProviderTools.installBCProvider();
            // createCRL prints info about crl generation
            try {
                final CAInfo caInfo = getCAInfo(getAuthenticationToken(), caName);
                if(caInfo == null) {
                    throw new CADoesntExistsException();
                }
                createCRL(caInfo.getSubjectDN(), deltaCrl);
            } catch (CADoesntExistsException e) {
                log.error("No CA named " + caName + " exists.");
                return CommandResult.FUNCTIONAL_FAILURE;
            }
        }
        return CommandResult.SUCCESS;
    }

    @Override
    public String getCommandDescription() {
        return "Issues a new CRL from the CA.";
    }

    @Override
    public String getFullHelpText() {
        return getCommandDescription();
    }
    
    @Override
    protected Logger getLogger() {
        return log;
    }

    @Override
    protected void createCRL(final String issuerdn, final boolean deltaCRL) {
        log.trace(">createCRL()");
        try {
            if (issuerdn != null) {
                CAInfo cainfo = EjbRemoteHelper.INSTANCE.getRemoteSession(CaSessionRemote.class).getCAInfo(getAuthenticationToken(),
                        issuerdn.hashCode());
                if (!deltaCRL) {
                    EjbRemoteHelper.INSTANCE.getRemoteSession(PublishingCrlSessionRemote.class).forceCRL(udate, getAuthenticationToken(), cainfo.getCAId());
                    int number = EjbRemoteHelper.INSTANCE.getRemoteSession(CrlStoreSessionRemote.class).getLastCRLNumber(issuerdn, CertificateConstants.NO_CRL_PARTITION, false);
                    log.info("CRL with number " + number + " generated.");
                } else {
                	if ( true ) throw new RuntimeException("not supported");
                    EjbRemoteHelper.INSTANCE.getRemoteSession(PublishingCrlSessionRemote.class).forceDeltaCRL(getAuthenticationToken(),
                            cainfo.getCAId());
                    int number = EjbRemoteHelper.INSTANCE.getRemoteSession(CrlStoreSessionRemote.class).getLastCRLNumber(issuerdn, CertificateConstants.NO_CRL_PARTITION, true);
                    log.info("Delta CRL with number " + number + " generated.");
                }
            } else {
            	if ( true ) throw new RuntimeException("not supported");
                int createdcrls = EjbRemoteHelper.INSTANCE.getRemoteSession(PublishingCrlSessionRemote.class).createCRLs(getAuthenticationToken()).size();
                log.info("  " + createdcrls + " CRLs have been created.");
                int createddeltacrls = EjbRemoteHelper.INSTANCE.getRemoteSession(PublishingCrlSessionRemote.class).createDeltaCRLs(
                        getAuthenticationToken()).size();
                log.info("  " + createddeltacrls + " delta CRLs have been created.");
            }
        } catch (Exception e) {
            log.error("Error while creating CRL for CA: " + issuerdn, e);
        }
        log.trace(">createCRL()");
    }

}
