/*************************************************************************
 *                                                                       *
 *  CESeCore: CE Security Core                                           *
 *                                                                       *
 *  This software is free software; you can redistribute it and/or       *
 *  modify it under the terms of the GNU Lesser General Public           *
 *  License as published by the Free Software Foundation; either         *
 *  version 2.1 of the License, or any later version.                    *
 *                                                                       *
 *  See terms of license at gnu.org.                                     *
 *                                                                       *
 *************************************************************************/
package org.cesecore.certificates.crl;

import java.util.Collection;

import javax.ejb.Local;

import org.cesecore.authentication.tokens.AuthenticationToken;
import org.cesecore.authorization.AuthorizationDeniedException;
import org.cesecore.certificates.ca.CA;
import org.cesecore.keys.token.CryptoTokenOfflineException;

/**
 * Local interface for CrlCreateSession
 * 
 * @version $Id: CrlCreateSessionLocal.java 17625 2013-09-20 07:12:06Z netmackan $
 *
 */
@Local
public interface CrlCreateSessionLocal extends CrlCreateSession {

    byte[] generateAndStoreCRL(java.util.Date udate, AuthenticationToken admin, CA ca, int crlPartitionIndex, Collection<RevokedCertInfo> certs, int basecrlnumber, int nextCrlNumber)
            throws CryptoTokenOfflineException, AuthorizationDeniedException;

}
