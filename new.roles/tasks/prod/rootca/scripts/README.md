## Introduction

The following script[s] provide installation and [eventual] maintenance of the RootCA.

* **main_firstinstall.sh**: Performs initial installation of EJBCA software with ManagementCA installed.  Result is running wildfly application in /data/software/wildfly and compile ejbca under /data/software/ejbca.  *This application fails if script has previously been run.  To rerun, delete /data/software directory and mariadb database.*



## Notes

* Scripts rely on vault and sudo access.  As such, each run, you'll be required to enter the vault password and sudo password.  While this can be scripted, for now, entering as part of runtime (no password visible) is the most straight forward path.

  * A test vault has been provided named vault_vars_test.yml.  Rename to vault_vars.yml for testing, and use the password Adminhjm

* Configuration requires /data/lib and /data/conf directory structure.

  * **/data/lib**: contains all non-os 'binary' libraries required by the build process.  These are define in the vars.yml.  This lib directory will be stored in git for CM (i know, artifactory would be better).
  * **/data/conf**: contains all 'text' files which override files during the build process.  This includes wildfly startup scripts, ejbca config files...etc.  This conf directory will be stored in git for CM.

  

## Outstanding:

- Add version numbers to yum installs
- Trust/keystore passwords should be stored in the vault.

