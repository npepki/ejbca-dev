# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi
# BEGIN ANSIBLE MANAGED BLOCK
PATH=$HOME/data/software/jdk/bin:~/data/software/ant/bin:$PATH
JAVA_HOME=$HOME/data/software/jdk
APPSRV_HOME=$HOME/data/software/ca
export PATH JAVA_HOME APPSRV_HOME
# END ANSIBLE MANAGED BLOCK
